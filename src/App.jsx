import React from 'react';
import ProductList from './components/ProductList';
import Button from './components/Button';
import Modal from './components/Modal';

import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import './App.scss';

class App extends React.Component {
  state = {
    products: [],
    cartItems: JSON.parse(localStorage.getItem('cartItems')) || [],
    favoriteItems: JSON.parse(localStorage.getItem('favoriteItems')) || [],
    showCartModal: false,
    showFavoritesModal: false
  };

  componentDidMount() {
    fetch('http://localhost:3000/products.json')
    .then(response => response.json())
    .then(products => {
      this.setState({ products });
    })
    .catch(error => {
      console.error('Error fetching products:', error);
    });

  }

  handleAddToCart = (product) => {
    const { cartItems } = this.state;
    const existingItemIndex = cartItems.findIndex(item => item.id === product.id);
    console.log(cartItems);
    if (existingItemIndex === -1) {
      const newItem = {
        id: product.id,
        name: product.name,
        price: product.price,
        image: product.image,
        quantity: 1
      };
      this.setState({ cartItems: [...cartItems, newItem] }, () => {
        localStorage.setItem('cartItems', JSON.stringify(this.state.cartItems));
      });
    } else {

      const updatedItem = {
        ...cartItems[existingItemIndex],
        quantity: cartItems[existingItemIndex].quantity + 1
      };

      const updatedCartItems = [...cartItems];

      updatedCartItems.splice(existingItemIndex, 1, updatedItem);
      this.setState({ cartItems: updatedCartItems }, () => {
        localStorage.setItem('cartItems', JSON.stringify(this.state.cartItems));
      });
    }
  }

  handleRemoveFromCart = (id) => {
    const { cartItems } = this.state;

    let updatedCartItems;

    const itemToEdit = cartItems.find((item) => item.id === id);

    if (itemToEdit.quantity === 1) {
      updatedCartItems = cartItems.filter(item => item.id !== id);
    } else {
      const updatedItem = {
        ...itemToEdit,
        quantity: itemToEdit.quantity - 1,
      };

      updatedCartItems = cartItems.map((item) => item.id === id ? updatedItem : item);
    }

    this.setState({ cartItems: updatedCartItems }, () => {
      localStorage.setItem('cartItems', JSON.stringify(this.state.cartItems));
    });
  }

  handleToggleCartModal = () => {
    this.setState({ showCartModal: !this.state.showCartModal });
  }

  handleAddToFavorites = (product) => {
    const { favoriteItems } = this.state;
    const existingItemIndex = favoriteItems.findIndex(item => item.id === product.id);

    if (existingItemIndex === -1) {
      const newItem = {
        id: product.id,
        name: product.name,
        price: product.price,
        image: product.image
      };
      this.setState({ favoriteItems: [...favoriteItems, newItem] }, () => {
        localStorage.setItem('favoriteItems', JSON.stringify(this.state.favoriteItems));
      });
    } else {
      const updatedFavoriteItems = favoriteItems.filter(item => item.id !== product.id);
      
      this.setState({ favoriteItems: updatedFavoriteItems }, () => {
        localStorage.setItem('favoriteItems', JSON.stringify(this.state.favoriteItems));
      });
    }
  }

  handleToggleFavoritesModal = () => {
    this.setState({ showFavoritesModal: !this.state.showFavoritesModal });
  }

  handleOutsideClick = (event) => {
    if (event.currentTarget === event.target) {
      this.setState({ 
        showCartModal: false,
        showFavoritesModal: false  
      });
    }
  };

  render() {
    const { products, cartItems, favoriteItems, showCartModal, showFavoritesModal } = this.state;
    const cartItemTotal = cartItems.reduce((total, item) => total + item.quantity, 0);
    const totalPrice = cartItems.reduce((total, item) => total + item.price * item.quantity, 0);
    const favoriteItemTotal = favoriteItems.length;

    return (
      <div className="app-wrapper">
        <header className="header">
          <div className="container header-wrapper">
            <h1 className="title">World of Funko POP!</h1>
            <div className="icons-wrapper">
              <div className="cart-shopping-wrapper" onClick={this.handleToggleCartModal}>
                <FontAwesomeIcon icon={faCartShopping} size="sm" style={{color: cartItems.length ? 'yellow' : 'blue' }} /> <span>{cartItemTotal}</span>
              </div>
              <div className="star-wrapper" onClick={this.handleToggleFavoritesModal}>
                <FontAwesomeIcon icon={faStar} size="sm" style={{outlineColor: "blue", color: favoriteItems.length ? 'yellow' : 'blue' }} /> <span>{favoriteItemTotal}</span>
              </div>
            </div>
          </div>
        </header>
        <ProductList
          products={products}
          cartItems={cartItems}
          favoriteItems={favoriteItems}
          AddToCart={this.handleAddToCart}
          RemoveFromCart={this.handleRemoveFromCart}
          AddToFavorites={this.handleAddToFavorites}
          showCartModal={this.handleToggleCartModal}
        />
        {showCartModal && 
          <Modal
            optionalСlassName="shoping-cart__modal"
            cartItems={cartItems} 
            closeModal={this.handleToggleCartModal}
            handleOutsideClick={this.handleOutsideClick}
            header="Кошик"
            text={
              !cartItems.length ?
                'Ваш кошик порожній.' :
                ''
              }
            actions={cartItems.map(item =>
              <div className="item-wrapper__modal" key={item.id}>
                <div className="item-name__modal">{item.name} ({item.quantity} x {item.price}грн)</div>
                <button onClick={() => this.handleRemoveFromCart(item.id)}>Видалити</button>
              </div>
            )}

            totalPrice={
            cartItems.length &&
              <div>Разом: {totalPrice} грн</div>
            }
          />
        }
        {showFavoritesModal && 
          <Modal
            optionalСlassName="favorites__modal" 
            favoriteItems={favoriteItems}
            closeModal={this.handleToggleFavoritesModal}
            handleOutsideClick={this.handleOutsideClick}
            header="Улюблені товари"

            text={
              !favoriteItems.length ?
                'У вас ще немає улюблених товарів.' :
                ''
              }
            actions={favoriteItems.map(item =>
              <div className="item-wrapper__modal" key={item.id}>
                <div className="item-name__modal">{item.name} - {item.price} грн</div>
                <button onClick={() => this.handleAddToCart(item)}>Додати в корзину</button>
              </div>
            )}
            totalPrice=""
          />
        }

      </div>
    );
  }
}

export default App;


