import React from 'react';
import PropTypes from 'prop-types';
import "./Button.scss";

class Button extends React.Component {
    render() {
        const {optionalClassName, backgroundColor, text, onClick } = this.props;

        return (
        <button
            className={`btn ${optionalClassName}`}
            style={{ backgroundColor }}
            onClick={onClick}
        >
            {text}
        </button>
        );
    }
}

Button.defaultProps = {
	type: 'button',
	click: () => {
	}
}

Button.propTypes = {
    optionalClassName: PropTypes.string,
    backgroundColor: PropTypes.string, 
    text: PropTypes.string, 
    onClick: PropTypes.func,
}

export default Button;