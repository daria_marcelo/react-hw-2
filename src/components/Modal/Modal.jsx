import React from "react";
import PropTypes from 'prop-types';
import "./Modal.scss";



class Modal extends React.Component {
   
    render() {
        const { header, optionalClassName, closeButton, totalPrice, text, actions, closeModal, handleOutsideClick } = this.props;
        return (
        <div className="modal-wrapper" onClick={handleOutsideClick}>
            <div className={`modal ${optionalClassName}`}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h2>{header}</h2>
                        {closeButton && (
                        <span className="modal-close" onClick={closeModal}>
                            &times;
                        </span>
                        )}
                    </div>
                    <div className="modal-body">
                        <p className="body-text">{text}</p>
                    </div>
                    <div className="modal-footer">{actions}</div>
                    {totalPrice}
                </div>
            </div>
        </div>
        );
    }
}

Modal.propTypes = {
    header: PropTypes.string, 
    closeButton: PropTypes.bool, 
    text: PropTypes.string,
    actions: PropTypes.arrayOf(
      PropTypes.object,
      PropTypes.node
    ),
    closeModal: PropTypes.func,
    handleOutsideClick: PropTypes.func,
}

export default Modal;
